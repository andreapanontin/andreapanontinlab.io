---
title: "About me"
date: 2022-03-13T22:16:37+01:00
draft: false
ShowBreadCrumbs: false
hidemeta: true
showtoc: false
math: true
---

I am a PhD student in [mathematics](https://www.uni-regensburg.de/mathematics/faculty/home/index.html) at [Universität Regensburg](https://www.uni-regensburg.de/en),
working under the supervision of
[Prof. Dr. Moritz Kerz](https://kerz.app.uni-regensburg.de/)
and [Dr. Veronika Ertl](https://ertlvroni.github.io/).   
I am supported by the [DFG](https://www.dfg.de/) through the project [SFB 1085 "Higher Invariants"](https://sfb-higher-invariants.app.uni-regensburg.de/index.php?title=Main_Page).

I am interested in arithmetic geometry,
especially \\(p\\)-adic cohomologies.


### Contacts
You can find me at the
[Faculty of Mathematics](https://www.uni-regensburg.de/mathematics/faculty/home/index.html)
of [Universität Regensburg](https://www.uni-regensburg.de/en).  
__E-mail__:
``andrea.panontin@mathematik.uni-regensburg.de``,  
__Office__: M310,  
__Phone__: +49 941 943 3142.


### CV
You can find the latest version of my CV
[here](https://andreapanontin.gitlab.io/CurriculumVitae/CV_Andrea_Panontin.pdf).

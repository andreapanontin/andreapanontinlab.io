---
title: "Photos"
draft: false
ShowBreadCrumbs: false
hidemeta: true
tocopen: false
description: "These are some significant photos I have taken over the years.
All of these are densely charged with meaning and emotion to me.
But beauty is in the eye of the beholder:
I hope that they can tell a little story also to you,
even if different from the one they evoke in me."
---

### Back to reality
![Back to reality: a skyline at dusk; the sky is tinted with a moltitude of colours, ranging from a soft red, to yellow, light blue and dark blue for the darker clouds. The main sight is a transmission tower, of which we can only see the black silouhette. The name of this picture is a reference to a turmoil of emotions that swept me, while I was "hiding" in nature, in the Mountains. The moment when I had to face my emotions was marked with coming back to the city, and made clear by a change of views.](1-BackToReality.jpg#center)
Monza (Italy), September 2018.

### Enchanting rest
![Enchanting rest: From a high viewpoint we can see a valley, surrounded by mountains on both sides. The clouds and the mountains are hidden behind a layer of cluds that gets less dense towards the ground, until, some meters above the viewer, they disperse almost completely. In the bottom center there is a parson, of whom we only see the back, enjoying our same view, in one of the few rests of his hike.](6-EnchantingRest.jpg#center)
Close to rifugio Galassi (Italy), August 2019.

### A new life
![A new life](2-ANewLife.jpg#center)
Shot by Giacomo Masiero in Parc de Peixotto, Talence (France), in October 2020.

### Dicono che c'è un tempo per aspettare
![Dicono che c'è un tempo per aspettare](3-DiconoCheC'èUnTempoPerAspettare.jpg#center)
Parco di Monza (Italy), February 2022.

### E uno per sognare
![E uno per sognare](4-EUnoPerSognare.jpg#center)
Bonneval-sur-Arc (France), August 2022.

### Wanderlust
![Wanderlust](5-Wanderlust.jpg#center)
Calalzo di Cadore (Italy), January 2023.

### RUSH!
![RUSH!: it's a picture of a sunset above a green flatland in Poland. In foreground we see mainly grass, a small road and a couple of transmission towers. Further away some trees hiding the horizon and the sun from our sight. This picture was taken from a moving train, which means that everything below the horizon looks blurry as it's blazing past the viewer and the camera, whereas the sky and clouds look still, showing how far they avtually are from us.](7-RUSH!.jpg#center)
Warsaw (Poland), May 2024.

---
title: "Reno"
date: 2022-09-09T17:30:18+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

O nubi, piangete con me, su questo Reno  
La fine di una lunga siccità.  
Celebrate la vita e le emozioni  
Distruggendo la normalità.  
Sotto queste lacrime alzo gli occhi:  
Quel nero che mi ha ultimamente spaventato  
È ora fonte di un incontenibile sorriso.  

Sospira assordante il vento  
La voce di mille ricordi.  
Limpidi e vermigli alimentano   
il fuoco che scalda il mio cuore.  
Ardono veloci e forte corrono  
Verso il passato, le loro scintille.  

In un attimo è già finito,  
È già sereno, è già notte.  
Il sole lascia riposare i miei occhi  
E il vento, placato, mi lascia sdraiare,  
Sfinito, su questo terreno  
Umido e fertile del mio pianto.  

Dopo la tempesta venne il sereno.  
Forse non bisogna contenere l'infinita  
Forza della natura, ma...  
Assecondarla?

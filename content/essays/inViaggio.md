---
title: "In viaggio"
date: 2022-10-02T13:30:00+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---
Come una freccia tra i pensieri  
Viaggio su queste lente rotaie  
Verso una meta a me sconosciuta.  

Fuori dal finestrino si alternano  
paesaggi e ricordi: li assaporo,  
riscaldato da una fitta pioggia  
di lacrime ed emozioni.  

Umida, la vista mi presenta  
Colori muti e scintillanti,  
Foglie dalle tinte cangianti  
E memorie di ogni sapore.  

E questa varietà mi travolge  
In un tumultuoso abbraccio,  
Che non mi lascerà stabile  
Ma mai mi farà sentire solo  

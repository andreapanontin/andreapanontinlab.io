---
title: "Healing"
date: 2022-06-06T12:12:45+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

Many people say that time heals everything, but I don't think so.
From what I've seen and experienced, time only makes all memories
less vivid, less fresh, less accurate.
Usually this has the consequence of making them align more
with our interpretation of the happenings.
In case the interpretation is linked to positive emotions, 
then the memory will align with it.
And the same will happen in case the memory gives rise to negative ones.
Though I wonder: is this a healing process?
I don't think so: there is no spontaneous switch of side.
If a memory was negative to begin with, time will leave it negative.
Analogously, if it was positive to begin with, no change of nature will happen.

Though, usually, older memories tend to fade away, what gives?
I don't think it is just due to time, more that it is linked to new experiences.
Living new, different experiences, developing a new reality and a new life,
will make older memories less relevant.
They will be thought about less often, they will not concern our life
as much as they used to: our brain will start reacting to them in a
duller, more dumbed down, way.

But anyhow, this is still not healing: again memories don't get associated to
different emotions, but to more distant ones.
So... Everybody talks about healing, what heals?
My guess is that understanding heals.
Most decisions that were carried out, by all involved parties,
were chosen because they were thought to be the best possibility
(given the limited available information).
Usually an emotional reaction is linked to the focus on the memory being
restricted to only a single side of the event, only usually to the self.
If somehow it was possible to consider all parties,
it would become evident how the choice was made in the (greatest)
interest of everyone involved.
And, even if that choice failed to achieve what it was set out for, 
the knowledge of what prompted it will make it possible to
look at the event and feel compassion, instead of whichever emotion
the event was previously linked to.

Of course this might not be enough. Sometimes the event will hurt too much,
sometimes it might be necessary to be heard, to explain to the other parties
why the event ended up being so painful and what it failed to satisfy.
In that case, the act of getting heard can help again, but I still have
to figure out why.

---
title: "Domani"
date: 2024-05-11T20:00:04+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

### Devi
Corriamo una corsa a tappe  
Disegnata prima del tempo  
Una corsa all'eternità e al potere  
Inseguendo, vento gelido in faccia,  
Un avversario imbattibile  
L'idealità  

### Puoi (non)
Tappa dopo tappa  
Sempre più lontana  
La testa della corsa  
È un miraggio  
irraggiungibile  
  
Ma in cerca di gloria  
Incollati al percorso  
Non ci poniamo domande  
Chiniamo la testa  
E cerchiamo invano  
Di scacciare il dolore  

### Vuoi
Ogni tappa più lunga  
Strappa ancora di più  
Dalle scarne riserve  
Di voracità  
  
Solo quando, finalmente,  
Giusto uno scheletro  
Ci fermeremo  
Riusciremo a smontare  
Questo circo perverso  
A mettere in dubbio  
Le idee senza cuore  
Che fanno correre tutti  
Verso una fine violenta  
Lontano dal proprio  
Desiderio d'amare

---
title: "Amata carta"
date: 2023-07-28T14:04:37+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Amata carta, temuti pensieri  
Tanto cari e preziosi, siete sovente,  
In una vita piena di eventi,  
Spaura di iati, distanti e assenti.

Il mare è calmo e io non me ne curo,  
Sempre inseguendo un futuro oscuro.  
Continuo ignaro e in lontananza  
Un'amorfa corrente cresce, avanza.

Tiranna e impetuosa in tempesta  
Si tramuta e verso riva si appresta.  
Le prime onde impattano violente  
Su una sponda ignara, interte.

Per contenerle, funziona niente.  
Nella paura sommersa e assorta  
Verso la carta, la mente smorta  
Vomita svelta l'imago opprimente

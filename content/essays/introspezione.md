---
title: "Introspezione"
date: 2023-06-17T16:24:53+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Un grido, un urlo  
Una richiesta d'aiuto

"Ti prego, perdonami"  
Piangendo: "ho fatto il mio meglio"  
"Vienimi incontro, hai chiesto troppo da me"  

Ma non potrà mai abbracciarti  
la tua immagine riflessa

---
title: "Fuoco"
date: 2022-05-24T11:13:21+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Nel cercare te   
ho trovato me stesso.

Conoscendo me, ho imparato a   
mettermi nei tuoi panni.

---
title: "Pilota automatico"
date: 2022-03-15T13:10:44+01:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---

Vuoto davanti agli occhi   
Nessun pensiero   
L'obiettivo è chiaro    
Ogni passo è già deciso    
È davanti a te    
Ma il piede non avanza   
L'occhio non vede   
Il cervello non elabora   

Bianco, neve.

### Paralisi
Cadere nelle vecchie abitudini   
Incapaci di scappare   
Travolti   
Dal peso di pensare      

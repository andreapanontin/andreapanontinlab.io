---
title: "Panta Rhei"
date: 2022-04-05T22:30:23+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Come travolto da una fredda corrente mi guardo intorno e tutto è cambiato.
Non riconosco più la vegetazione che abbraccia il flusso dei miei pensieri.
La corrente è forte, ma come sempre continuo a nuotare per non affogare.
Non mi sono mai veramente chiesto perché, forse questo è il bello.
Ma i miei gesti, a differenza del paesaggio, sono sempre gli stessi.
Semplicemente ogni movimento porta con sé un immenso significato.
Ogni respiro, ogni muscolo che si contrae, ogni pensiero è arricchito
dall'abbraccio di una persona con cui l'ho condiviso.
E questo calore mi protegge dall'assiderare, preserva le poche
energie che ho e mi permette di continuare a nuotare.


### Translation
As if swept away by a cold current, I look around and everything
has changed.
I cannot recognize the vegetation that embraces my thoughts anymore.
The current is strong but, just like always, I keep swimming, not to drown.
I've never asked myself why, maybe that is the nice part about it.
Though, unlike the landscape, all my gestures remain the same.
Simply every movement carries immense meaning.
Every breath, every muscle contraction, every thought is enriched
by the embrace of a person with whom I shared it.
And this warmth protects me from freezing, it preservers the little
remaining energy I have and allows me to keep swimming.

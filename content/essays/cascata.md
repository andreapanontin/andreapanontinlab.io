---
title: "Cascata"
date: 2022-08-21T20:30:56+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---
Passeggia, blu intensa  
Piano, giù per il  
Dolce pendio  

Prendi ordinata  
Velocità, abbracciando  
Le verdi rocce  

Cavalca giù dal dirupo  
Increspando il tuo  
Bianco crine  
E impatta sulle rocce,  
Lanciandomi verso il cielo  
Libero dal peso dei pensieri  
Sovraffatto dalla tua forza  
Sovraumana  

Lasciami librare  
Tra le nuvole,  
Permettimi di guardare  
Gli uomini, così piccoli  

Ogni cruccio, da qua su,  
Sotto il tuo impeto  
Appare futile,  
Sembra scomparire   

Conducimi con questo  
Galoppo alato  
Oltre il cratere  
Ove trionfa  
Il vento

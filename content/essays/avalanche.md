---
title: "Avalanche"
date: 2023-01-30T10:24:56+01:00
ShowReadingTime: false
hideSummary: true
showtoc: false
---

Living life is similar to a hike in winter:
you will face a lot of cold and wind,
and it takes experience to learn how to cover
oneself up and shed from the elements.
Comrades make the journey less lonely, usually.

Depending on the terrain one is on,
it is important to take care of each step:
a false step and you might fall off a cliff.
But nature can be hideous: you could walk in a
beautiful, quite place, without noticing that at
each step the risk for an avalanche is increasing.
Every step might bring you closer to doom,
and not a single signal might warn you.

It is only when the snow front detaches that things
appear in their whole gravity. It might be too late,
you might be buried in the snow, or you might be able
to escape, even unharmed.
Sometimes it's the presence of friends surrounding us
the only chance of getting out of it,
sometimes even their presence is not enough.

But the shocking, cold feeling of the mountain of
snow treading towards you will linger in your soul.
It's only after you've warmed up again that you will be
able to think back at the events, hoping to
find out the root of the risk.
It's going to take time to find confidence again
to go and hike for these beautiful views,
and only time and thought (or maybe a good deal of luck)
will turn this pilgrimage of dangers
into a safe trail of beauty.

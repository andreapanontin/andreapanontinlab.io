---
title: "Attacco di panico"
date: 2024-01-26T19:07:29+02:00
ShowReadingTime: false
hideSummary: true
showtoc: false
draft: false
---
Sale la nebbia, fino a diventare così densa
da non distinguere più i tuoi pensieri.
Anche i tuoi passi rallentano, ti senti stanco, freddo.  
Ti siedi e tutto intorno colori,
suoni ed emozioni ti attraversano.
Un'immagine dopo l'altra, incontrollate,
ti tolgono il fiato.
Quando sono riuscito a prendere le redini
della mia persona l'ultima volta?  
Perdi, con i tuoi pensieri, una forma,
schiacciato sotto le raffiche incessanti...
Fino a che non smetti di resistere e
finalmente permetti al vento di spargerti,
inerme, nell'immensità dei tuoi timori,
delle tue incertezze.

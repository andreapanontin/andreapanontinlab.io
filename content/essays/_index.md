---
title: "Diary of internal displacement"
date: 2022-05-25T16:10:00+02:00
ShowBreadCrumbs: false
---

This is a collection of texts of various
nature that I have written over the years.
They were meant to free my mind from besieging thoughts,
by putting them into words.
> No claim of originality is made except for errors.
